<?php

namespace Crija\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Knp\Snappy\Pdf;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Template("CrijaFrontendBundle:Default:index.html.twig")
     */
    public function indexAction()
    {
        $this->em = $this->get('doctrine')->getManager();

        $sql2 = "SELECT sum(litres) FROM aquarium";
        $stmt = $this->em->getConnection()->prepare($sql2);
        $stmt->execute();
        $litres = $stmt->fetchColumn();

        $sql2 = "SELECT count(id) FROM fos_user";
        $stmt = $this->em->getConnection()->prepare($sql2);
        $stmt->execute();
        $users = $stmt->fetchColumn();

        $sql2 = "SELECT count(id) FROM water_parameter";
        $stmt = $this->em->getConnection()->prepare($sql2);
        $stmt->execute();
        $meditions = $stmt->fetchColumn();


        $sql2 = "SELECT count(id) FROM aquarium";
        $stmt = $this->em->getConnection()->prepare($sql2);
        $stmt->execute();
        $aquariums = $stmt->fetchColumn();

        $sql2 = "SELECT count(id) FROM Tenant";
        $stmt = $this->em->getConnection()->prepare($sql2);
        $stmt->execute();
        $tenants = $stmt->fetchColumn();


        return array(
            'aquariums' => $aquariums,
            'litres' => $litres,
            'meditions' => $meditions,
            'tenants' => $tenants,
            'users' => $users
        );
    }

    /**
     * Widget
     *
     * @Route("/estadisticas_globales", name="global_widget_parameter")
     * @Template("CrijaAquariumBundle:WaterParameter:global.html.twig")
     */
    public function globalWidgetAction()
    {
        $this->em = $this->get('doctrine')->getManager();

        $sql = "SELECT FROM_DAYS( TO_DAYS( created_at ) - MOD( TO_DAYS( created_at ) -1, 7 ) ) AS semana, count( id ) AS mediciones, sum( salinity ) total, ROUND( sum( salinity ) / count( id ) ) AS media
                FROM water_parameter
                WHERE salinity IS NOT NULL
                 AND created_at > '2015-03-15'
                AND created_at > extract(
                YEAR_MONTH FROM CURRENT_DATE - INTERVAL 1
                MONTH )
                GROUP BY FROM_DAYS( TO_DAYS( created_at ) - MOD( TO_DAYS( created_at ) -1, 7 ) )";


        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        $salinity = $stmt->fetchAll();


        $sql = "SELECT FROM_DAYS( TO_DAYS( created_at ) - MOD( TO_DAYS( created_at ) -1, 7 ) ) AS semana, count( id ) AS mediciones, sum( temperature ) total, ROUND( sum( temperature ) / count( id ) ) AS media
                FROM water_parameter
                WHERE temperature IS NOT NULL
                AND created_at > '2015-03-15'
                AND created_at > extract(
                YEAR_MONTH FROM CURRENT_DATE - INTERVAL 1
                MONTH )
                GROUP BY FROM_DAYS( TO_DAYS( created_at ) - MOD( TO_DAYS( created_at ) -1, 7 ) )";


        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        $temperature = $stmt->fetchAll();

        $sql = "SELECT FROM_DAYS( TO_DAYS( created_at ) - MOD( TO_DAYS( created_at ) -1, 7 ) ) AS semana, count( id ) AS mediciones, sum( ph ) total, ROUND( sum( ph ) / count( id ) ) AS media
                FROM water_parameter
                WHERE ph IS NOT NULL
                 AND created_at > '2015-03-15'
                AND created_at > extract(
                YEAR_MONTH FROM CURRENT_DATE - INTERVAL 1
                MONTH )
                GROUP BY FROM_DAYS( TO_DAYS( created_at ) - MOD( TO_DAYS( created_at ) -1, 7 ) )";


        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        $ph_data = $stmt->fetchAll();


        foreach($salinity as $sal) {
            if(!isset($data[$sal['semana']]['temp'])) {
                $data[$sal['semana']]['temp'] = null;
            }
            if(!isset($data[$sal['semana']]['ph'])) {
                $data[$sal['semana']]['ph'] = null;
            }
            $data[$sal['semana']]['semana'] = $sal['semana'];
            $data[$sal['semana']]['sal'] = $sal['media'];
        }

        foreach($temperature as $tmp) {
            if(!isset($data[$tmp['semana']]['sal'])) {
                $data[$tmp['semana']]['sal'] = null;
            }
            if(!isset($data[$tmp['semana']]['ph'])) {
                $data[$tmp['semana']]['ph'] = null;
            }

            $data[$tmp['semana']]['semana'] = $tmp['semana'];
            $data[$tmp['semana']]['temp'] = $tmp['media'];
        }


        foreach($ph_data as $ph) {
            if(!isset($data[$ph['semana']]['sal'])) {
                $data[$ph['semana']]['sal'] = null;
            }
            if(!isset($data[$ph['semana']]['temp'])) {
                $data[$ph['semana']]['temp'] = null;
            }

            $data[$ph['semana']]['semana'] = $ph['semana'];
            $data[$ph['semana']]['ph'] = $ph['media'];
        }


        $sql = "SELECT FROM_DAYS( TO_DAYS( created_at ) - MOD( TO_DAYS( created_at ) -1, 7 ) ) AS semana, count( id ) AS mediciones, sum( nitrite ) total,  round(sum( nitrite)  / count( id ),3)  AS media
                FROM water_parameter
                WHERE nitrite IS NOT NULL
                 AND created_at > '2015-03-15'
                AND created_at > extract(
                YEAR_MONTH FROM CURRENT_DATE - INTERVAL 1
                MONTH )
                GROUP BY FROM_DAYS( TO_DAYS( created_at ) - MOD( TO_DAYS( created_at ) -1, 7 ) )";

        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        $nitrite = $stmt->fetchAll();

        $sql = "SELECT FROM_DAYS( TO_DAYS( created_at ) - MOD( TO_DAYS( created_at ) -1, 7 ) ) AS semana, count( id ) AS mediciones, sum( nitrate ) total,  round(sum( nitrate ) / count( id ),3)  AS media
                FROM water_parameter
                WHERE nitrate IS NOT NULL
                AND created_at > '2015-03-15'
                AND created_at > extract(
                YEAR_MONTH FROM CURRENT_DATE - INTERVAL 1
                MONTH )
                GROUP BY FROM_DAYS( TO_DAYS( created_at ) - MOD( TO_DAYS( created_at ) -1, 7 ) )";

        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        $nitrate = $stmt->fetchAll();

        $sql = "SELECT FROM_DAYS( TO_DAYS( created_at ) - MOD( TO_DAYS( created_at ) -1, 7 ) ) AS semana, count( id ) AS mediciones, sum( phosphate ) total, round(sum( phosphate ) / count( id ),3) AS media
                FROM water_parameter
                WHERE phosphate IS NOT NULL
                 AND created_at > '2015-03-15'
                AND created_at > extract(
                YEAR_MONTH FROM CURRENT_DATE - INTERVAL 1
                MONTH )
                GROUP BY FROM_DAYS( TO_DAYS( created_at ) - MOD( TO_DAYS( created_at ) -1, 7 ) )";


        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        $phosphate = $stmt->fetchAll();



        foreach($nitrite as $nitri) {
            if(!isset($data2[$nitri['semana']]['nitrate'])) {
                $data2[$nitri['semana']]['nitrate'] = null;
            }
            if(!isset($data[$nitri['semana']]['phosphate'])) {
                $data2[$nitri['semana']]['phosphate'] = null;
            }
            $data2[$nitri['semana']]['semana'] = $nitri['semana'];
            $data2[$nitri['semana']]['nitrite'] = $nitri['media'];
        }

        foreach($nitrate as $nitra) {
            if(!isset($data2[$nitra['semana']]['nitrite'])) {
                $data2[$nitra['semana']]['nitrite'] = null;
            }
            if(!isset($data2[$nitra['semana']]['phosphate'])) {
                $data2[$nitra['semana']]['phosphate'] = null;
            }

            $data2[$nitra['semana']]['semana'] = $nitra['semana'];
            $data2[$nitra['semana']]['nitrate'] = $nitra['media'];
        }


        foreach($phosphate as $ph) {
            if(!isset($data2[$ph['semana']]['nitrate'])) {
                $data2[$ph['semana']]['nitrate'] = null;
            }
            if(!isset($data2[$ph['semana']]['nitrite'])) {
                $data2[$ph['semana']]['nitrite'] = null;
            }

            $data2[$ph['semana']]['semana'] = $ph['semana'];
            $data2[$ph['semana']]['phosphate'] = $ph['media'];
        }


        $sqlAnimal =  "SELECT count( id ) AS numero, animal_id
                        FROM Tenant
                        GROUP BY animal_id
                        ORDER BY numero DESC
                        LIMIT 20";

        $stmt = $this->em->getConnection()->prepare($sqlAnimal);
        $stmt->execute();
        $animals_data = $stmt->fetchAll();


        foreach($animals_data as $animal_data) {

            $animal = $this->em->getRepository('CrijaAnimalBundle:Animal')->findOneBy(array('id'=> $animal_data['animal_id']));
            $animals[] = $animal;
        }

        return array(
            'data'      => $data,
            'data2'     => $data2,
            'animals'   => $animals
        );
    }


    /**
     * @Route("/contact", name="contact")
     * @Template("")
     */
    public function contactAction()
    {
        return array();
    }
    /**
     * widget for forum.
     *
     * @Route("/my_widget/{id}", name="my_widget")
     * @Template()
     */
    public function myWidgetAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $keys = array('salinity','calcium','ph','temperature','magnesium','phosphate','alkalinity','ammonia','silica','iodine','nitrate','nitrite','boron','iron','strontium','potassium');



        $aquariums = $em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($id);

        $ultima_medicion = array();

        foreach($aquariums as $aquarium) {

            $has_parameters = $em->getRepository('CrijaAquariumBundle:WaterParameter')->findBy(array("aquarium" => $aquarium));
            if(count($has_parameters) > 0)
            {
                foreach($keys as $key) {
                    $parameters = $em->getRepository('CrijaAquariumBundle:WaterParameter')->getLastValuesByKey($aquarium,$key,2);

                    $parameter_last[$key] = $parameters[0];
                    if($parameters[1] != null ) {
                        $parameter_before[$key] = $parameters[1];
                    }
                    else
                    {
                        $parameter_before[$key] = $parameters[0];
                    }
                }
                $aquariums_array[] = array("last" => $parameter_last,"before" => $parameter_before,"name" => $aquarium->getName());

            }
            else {
                $aquariums_array[] = array("last" => null,"before" => null,"name" => $aquarium->getName());
            }
        }

        return array(
            'aquariums'      => $aquariums_array,
        );




    }
    /**
     * generate image
     *
     * @Route("/my_widget_image", name="my_widget_image")
     * @Template()
     */
    public function myWidgetImageAction()
    {

        $output = shell_exec('wkhtmltoimage http://mycoralreef.es/my_widget widgets/test.png');

    }

}
