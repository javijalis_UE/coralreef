<?php

namespace Crija\Bundle\AnimalBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityRepository;


class TenantType extends AbstractType
{
    private $securityContext;

    public function __construct(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $user = $this->securityContext->getToken()->getUser();
        $user_id = (int)$user->getId();

        $builder
            ->add('description', 'textarea', array('required'  => false,
                'attr' => array('cols' => '148', 'rows' => '3')))
            ->add('inReefAt', null, array('widget' => 'single_text','data'  => date_create()))
            ->add('size', 'choice', array(
                'choices'   => array('M' => 'Mediano', 'S' => 'Pequeño', 'XL' => 'Grande'),
                'required'  => false
            ))
            ->add('price')
            ->add('units')
            ->add('aquarium', 'entity',
                array(
                    'class'         => 'CrijaAquariumBundle:Aquarium',
                    'query_builder' => function(EntityRepository $er) use ($user_id) {
                        return $er->createQueryBuilder('i')->andWhere('i.user = :user_id')->setParameter('user_id', $user_id);
                    }
                )
            )
            ->add('animal', 'choice', array(
                'choices'   => array(
                    '0'   => 'Selecciona inquilino',
                ),
                'data' => 0,
            ));

    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Crija\Bundle\AnimalBundle\Entity\Tenant'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'crija_bundle_animalbundle_tenant';
    }
}
