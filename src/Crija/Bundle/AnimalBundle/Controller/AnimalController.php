<?php

namespace Crija\Bundle\AnimalBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Crija\Bundle\AnimalBundle\Entity\Animal;
use Crija\Bundle\AnimalBundle\Form\AnimalType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Animal controller.
 *
 * @Route("/animal")
 */
class AnimalController extends Controller
{

    /**
     * Lists all Animal entities.
     *
     * @Route("/page/{page}",defaults={"page" = 1}, name="animal")
     * @Template()
     */
    public function indexAction($page,Request $request )
    {
        $em = $this->getDoctrine()->getManager();

        $pageSize = 10;
        $repository = $em->getRepository('CrijaAnimalBundle:Animal');


        $filter = $request->request->get('filter');

        $qb = $repository->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        if($filter) {
            $qb = $qb->Where("a.realName like '%".$filter."%'");
            $qb = $qb->orWhere("a.englishName like '%".$filter."%'");
            $qb = $qb->orWhere("a.spanishName like '%".$filter."%'");
        }


        $total = $qb->getQuery()->getSingleScalarResult();

        $query = $repository->createQueryBuilder('p');

        if($filter) {
            $query = $query->Where("p.realName like '%".$filter."%'");
            $query = $query->orWhere("p.englishName like '%".$filter."%'");
            $query = $query->orWhere("p.spanishName like '%".$filter."%'");
        }
            $query = $query->setFirstResult($pageSize * ($page - 1))
                ->setMaxResults(30)
                ->getQuery();


        $entities = $query->getResult();


        $total_pages = ceil($total/$pageSize);

        return array(
            'entities' => $entities,
            'total' => $total,
            'total_pages' => $total_pages,
            'page' => $page,
            'filter' => $filter
        );
    }

    /**
     * sitemap fort animals
     *
     * @Route("/animal_sitemap.xml", name="animal_sitemap")
     * @Method("GET")
     * @Template()
     */
    public function animalSitemapAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('CrijaAnimalBundle:Animal')->findAll();


        return array(
            'entities' => $entities,
        );


    }
    /**
     * Lists all animals in json
     *
     * @Route("/all_animals", name="all_animals")
     * @Method("GET")
     * @Template()
     */
    public function allAnimalsAction(Request $request)
    {
        $logger = $this->get('logger');
        $user = $this->get('security.context')->getToken()->getUser();
        $word = $request->get('q');

        $logger->error('----> buscando animal: '.$user->getUsername()." (".$user->getId().") ".$_SERVER['HTTP_USER_AGENT']);
        $logger->error('----> buscando animal: '.$word);

        $animals = array();
        $em = $this->getDoctrine()->getManager();

        // $entities = $em->getRepository('CrijaAnimalBundle:Animal')->findAll();

        $repository = $em->getRepository('CrijaAnimalBundle:Animal');
        $query = $repository->createQueryBuilder('p')
            ->select('c','p')
            ->leftJoin('p.animal_category', 'c')
            ->where('p.realName LIKE :word')
            ->orWhere('p.englishName LIKE :word')
            ->orWhere('p.spanishName LIKE :word')
            ->setParameter('word', '%'.$word.'%')
            ->getQuery();

        $entities = $query->getArrayResult();

        foreach($entities as $entity){

            $animals['animals'][] = array('id' => $entity['id'],
                'image' => $entity['image'],'english_name' => $entity['englishName'],
                'spanish_name' => $entity['spanishName'],
                'real_name' => $entity['realName'],
                'description' => $entity['description'],
                'fishgroup' => $entity['fishGroup'],
                'reefsafe' => $entity['reefsafe'],
                'dificulty' => $entity['dificulty'],
                'size' => $entity['size'],
                'fuente' => $entity['fuente'],
                'category' => $entity['animal_category']['name']);


        }

        if(count($entities) == 0) {
            $response = array("total_count" => 0, "incomplete_results" => false,"animals" => $animals);
        }
        else {
            $response = $animals;
        }
        $logger->error(serialize($response));
        return new JsonResponse($response);
    }



    /**
     * Creates a new Animal entity.
     *
     * @Route("/", name="animal_create")
     * @Method("POST")
     * @Template("CrijaAnimalBundle:Animal:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Animal();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->upload();
            $entity->setActive(true);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tenant_new',array('msg'=>"El animal ".$entity->getRealName()." se ha dado de alta correctamente, ya puedes añadirlo como inquilino")));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Animal entity.
     *
     * @param Animal $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Animal $entity)
    {
        $form = $this->createForm(new AnimalType(), $entity, array(
            'action' => $this->generateUrl('animal_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Animal entity.
     *
     * @Route("/new", name="animal_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Animal();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Animal entity.
     *
     * @Route("/ficha/{slug}-{spanishName}", name="animal_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($slug,$spanishName)
    {
        $name = urldecode($slug);
        $spanishName = urldecode($spanishName);

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaAnimalBundle:Animal')->findOneBy(array('realName'=> $name,'spanishName'=> $spanishName));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Animal entity.');
        }



        return array(
            'entity'      => $entity,

        );
    }

    /**
     * Displays a form to edit an existing Animal entity.
     *
     * @Route("/{id}/edit", name="animal_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaAnimalBundle:Animal')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Animal entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Animal entity.
    *
    * @param Animal $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Animal $entity)
    {
        $form = $this->createForm(new AnimalType(), $entity, array(
            'action' => $this->generateUrl('animal_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Animal entity.
     *
     * @Route("/{id}", name="animal_update")
     * @Method("PUT")
     * @Template("CrijaAnimalBundle:Animal:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaAnimalBundle:Animal')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Animal entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

      //  if ($editForm->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $entity->upload();
            $entity->setActive(true);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('animal_edit', array('id' => $id)));
       // }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Animal entity.
     *
     * @Route("/delete/{id}", name="animal_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if (!$user) {
            throw $this->createNotFoundException('Imposible localizar al usuario');
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CrijaAnimalBundle:Animal')->find($id);
        ldd($entity->getUser->getId());
        if($entity->getUser->getId() != $user->getId()) {
            throw $this->createNotFoundException('Operacion denegada');
        }

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Animal entity.');
            }

            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('animal'));
    }

    /**
     * Creates a form to delete a Animal entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('animal_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Escalar imagenes
     */
    private function scaleImageAction($imagen)
    {


        $ruta_imagen = $imagen;
        $escala = 0.50;

        $info_fuente = getimagesize($ruta_imagen);
        $recurso_fuente = imagecreatefromjpeg($ruta_imagen);

        $ancho_nuevo = round($info_fuente[0] * $escala);
        $alto_nuevo = round($info_fuente[1] * $escala);
        $tipo_mime = $info_fuente['mime'];

        $recurso_copia = imagecreatetruecolor($ancho_nuevo, $alto_nuevo);

        imagecopyresampled($recurso_copia, $recurso_fuente, 0, 0, 0, 0,
            $ancho_nuevo, $alto_nuevo,
            $info_fuente[0], $info_fuente[1]);


        header('Content-type: ' . $tipo_mime);
        imagejpeg($recurso_copia, NULL, 100);
        imagedestroy($recurso_copia);
        imagedestroy($recurso_fuente);
    }


}
