<?php

namespace Crija\Bundle\AnimalBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Crija\Bundle\AnimalBundle\Entity\AnimalCategory;
use Crija\Bundle\AnimalBundle\Form\AnimalCategoryType;

/**
 * AnimalCategory controller.
 *
 * @Route("/animalcategory")
 */
class AnimalCategoryController extends Controller
{

    /**
     * Lists all AnimalCategory entities.
     *
     * @Route("/", name="animalcategory")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CrijaAnimalBundle:AnimalCategory')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new AnimalCategory entity.
     *
     * @Route("/", name="animalcategory_create")
     * @Method("POST")
     * @Template("CrijaAnimalBundle:AnimalCategory:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new AnimalCategory();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('animalcategory_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a AnimalCategory entity.
     *
     * @param AnimalCategory $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(AnimalCategory $entity)
    {
        $form = $this->createForm(new AnimalCategoryType(), $entity, array(
            'action' => $this->generateUrl('animalcategory_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new AnimalCategory entity.
     *
     * @Route("/new", name="animalcategory_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new AnimalCategory();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a AnimalCategory entity.
     *
     * @Route("/{id}", name="animalcategory_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaAnimalBundle:AnimalCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnimalCategory entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing AnimalCategory entity.
     *
     * @Route("/{id}/edit", name="animalcategory_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaAnimalBundle:AnimalCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnimalCategory entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a AnimalCategory entity.
    *
    * @param AnimalCategory $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(AnimalCategory $entity)
    {
        $form = $this->createForm(new AnimalCategoryType(), $entity, array(
            'action' => $this->generateUrl('animalcategory_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing AnimalCategory entity.
     *
     * @Route("/{id}", name="animalcategory_update")
     * @Method("PUT")
     * @Template("CrijaAnimalBundle:AnimalCategory:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaAnimalBundle:AnimalCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnimalCategory entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('animalcategory_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a AnimalCategory entity.
     *
     * @Route("/{id}", name="animalcategory_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CrijaAnimalBundle:AnimalCategory')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AnimalCategory entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('animalcategory'));
    }

    /**
     * Creates a form to delete a AnimalCategory entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('animalcategory_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
