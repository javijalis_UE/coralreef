<?php

namespace Crija\Bundle\AnimalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Animal
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Crija\Bundle\AnimalBundle\Entity\AnimalRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Animal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="real_name", type="string", length=255)
     */
    private $realName;

    /**
     * @var string
     *
     * @ORM\Column(name="english_name", type="string", length=255, nullable = true)
     */
    private $englishName;

    /**
     * @var string
     *
     * @ORM\Column(name="spanish_name", type="string", length=255, nullable = true)
     */
    private $spanishName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable = true)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable = true)
     */
    private $active;

    /**
     * @var string
     * @Assert\File(maxSize="6000000")
     * @ORM\Column(name="image", type="string", length=255, nullable = true)
     */
    private $image;


    /**
     * @Orm\ManyToOne(targetEntity="AnimalCategory", inversedBy="animals")
     * @Orm\JoinColumn(name="animal_category_id", referencedColumnName="id")
     **/
    private $animal_category;

    /**
     * @Orm\OneToMany(targetEntity="Tenant", mappedBy="animal")
     **/
    private $tenants;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="fishGroup", type="string", length=255, nullable = true)
     */
    private $fishGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=255, nullable = true)
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="sex", type="string", length=255, nullable = true)
     */
    private $sex;

    /**
     * @var string
     *
     * @ORM\Column(name="dificulty", type="string", length=255, nullable = true)
     */
    private $dificulty;

    /**
     * @var string
     *
     * @ORM\Column(name="temperament", type="string", length=255, nullable = true)
     */
    private $temperament;

    /**
     * @var string
     *
     * @ORM\Column(name="volume", type="string", length=255, nullable = true)
     */
    private $volume;

    /**
     * @var string
     *
     * @ORM\Column(name="reefsafe", type="string", length=255, nullable = true)
     */
    private $reefsafe;


    /**
     * @var string
     *
     * @ORM\Column(name="feeding", type="text", nullable = true)
     */
    private $feeding;

    /**
     * @var string
     *
     * @ORM\Column(name="fuente", type="text", nullable = true)
     */
    private $fuente;

    /**
     * @var string
     *
     * @ORM\Column(name="behavior", type="text", nullable = true)
     */
    private $behavior;




    public function __construct() {
        $this->tentants = new ArrayCollection();

    }

    public function __toString() {
       return $this->name." (".$this->realName.")";

    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set realName
     *
     * @param string $realName
     * @return Animal
     */
    public function setRealName($realName)
    {
        $this->realName = $realName;

        return $this;
    }

    /**
     * Get realName
     *
     * @return string 
     */
    public function getRealName()
    {
        return $this->realName;
    }

    /**
     * Set englishName
     *
     * @param string $englishName
     * @return Animal
     */
    public function setEnglishName($englishName)
    {
        $this->englishName = $englishName;

        return $this;
    }

    /**
     * Get englishName
     *
     * @return string 
     */
    public function getEnglishName()
    {
        return $this->englishName;
    }

    /**
     * Set spanishName
     *
     * @param string $spanishName
     * @return Animal
     */
    public function setSpanishName($spanishName)
    {
        $this->spanishName = $spanishName;

        return $this;
    }

    /**
     * Get spanishName
     *
     * @return string
     */
    public function getSpanishName()
    {
        return $this->spanishName;
    }
    /**
     * Set description
     *
     * @param string $description
     * @return Animal
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Animal
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set animal_category
     *
     * @param \Crija\Bundle\AnimalBundle\Entity\AnimalCategory $animalCategory
     * @return Animal
     */
    public function setAnimalCategory(\Crija\Bundle\AnimalBundle\Entity\AnimalCategory $animalCategory = null)
    {
        $this->animal_category = $animalCategory;

        return $this;
    }

    /**
     * Get animal_category
     *
     * @return \Crija\Bundle\AnimalBundle\Entity\AnimalCategory 
     */
    public function getAnimalCategory()
    {
        return $this->animal_category;
    }

    /**
     * Add tenants
     *
     * @param \Crija\Bundle\AnimalBundle\Entity\Tenant $tenants
     * @return Animal
     */
    public function addTenant(\Crija\Bundle\AnimalBundle\Entity\Tenant $tenants)
    {
        $this->tenants[] = $tenants;

        return $this;
    }

    /**
     * Remove tenants
     *
     * @param \Crija\Bundle\AnimalBundle\Entity\Tenant $tenants
     */
    public function removeTenant(\Crija\Bundle\AnimalBundle\Entity\Tenant $tenants)
    {
        $this->tenants->removeElement($tenants);
    }

    /**
     * Get tenants
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTenants()
    {
        return $this->tenants;
    }

    /**
     * Add shops
     *
     * @param \Crija\Bundle\AnimalBundle\Entity\Shop $shops
     * @return Animal
     */
    public function addShop(\Crija\Bundle\AnimalBundle\Entity\Shop $shops)
    {
        $this->shops[] = $shops;

        return $this;
    }

    /**
     * Remove shops
     *
     * @param \Crija\Bundle\AnimalBundle\Entity\Shop $shops
     */
    public function removeShop(\Crija\Bundle\AnimalBundle\Entity\Shop $shops)
    {
        $this->shops->removeElement($shops);
    }

    /**
     * Get shops
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getShops()
    {
        return $this->shops;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * @return string
     */
    public function getBehavior()
    {
        return $this->behavior;
    }

    /**
     * @param string $behavior
     */
    public function setBehavior($behavior)
    {
        $this->behavior = $behavior;
    }

    /**
     * @return string
     */
    public function getDificulty()
    {
        return $this->dificulty;
    }

    /**
     * @param string $dificulty
     */
    public function setDificulty($dificulty)
    {
        $this->dificulty = $dificulty;
    }

    /**
     * @return string
     */
    public function getFeeding()
    {
        return $this->feeding;
    }

    /**
     * @param string $feeding
     */
    public function setFeeding($feeding)
    {
        $this->feeding = $feeding;
    }

    /**
     * @return string
     */
    public function getFuente()
    {
        return $this->fuente;
    }

    /**
     * @param string $fuente
     */
    public function setFuente($fuente)
    {
        $this->fuente = $fuente;
    }

    /**
     * @return string
     */
    public function getFishGroup()
    {
        return $this->fishGroup;
    }

    /**
     * @param string $fishGroup
     */
    public function setFishGroup($fishGroup)
    {
        $this->fishGroup = $fishGroup;
    }

    /**
     * @return string
     */
    public function getReefsafe()
    {
        return $this->reefsafe;
    }

    /**
     * @param string $reefsafe
     */
    public function setReefsafe($reefsafe)
    {
        $this->reefsafe = $reefsafe;
    }

    /**
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param string $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }

    /**
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param string $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getTemperament()
    {
        return $this->temperament;
    }

    /**
     * @param string $temperament
     */
    public function setTemperament($temperament)
    {
        $this->temperament = $temperament;
    }

    /**
     * @return string
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * @param string $volume
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;
    }
    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getAbsolutePath()
    {
        return null === $this->image
            ? null
            : $this->getUploadRootDir().'/'.$this->image;
    }

    public function getWebPath()
    {
        return null === $this->image
            ? null
            : $this->getUploadDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/animal';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        $sha_name = sha1(uniqid(mt_rand(), true)).".".$this->getFile()->getClientOriginalExtension();
        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
       // ldd($this->getFile());
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $sha_name
        );

        // set the path property to the filename where you've saved the file
        $this->image = $sha_name;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }
}
