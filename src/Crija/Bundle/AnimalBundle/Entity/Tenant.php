<?php

namespace Crija\Bundle\AnimalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tenant
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Crija\Bundle\AnimalBundle\Entity\TenantRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Tenant
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="colors", type="text",nullable=true)
     */
    private $colors;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text",nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="in_reef_at", type="datetime")
     */
    private $inReefAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="died_at", type="datetime" ,nullable=true)
     */
    private $diedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=255, nullable=true)
     */
    private $size;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;


    /**
     * @var integer
     *
     * @ORM\Column(name="units", type="integer", nullable=true)
     */
    private $units;



    /**
     * @Orm\ManyToOne(targetEntity="Animal", inversedBy="tenants")
     * @Orm\JoinColumn(name="animal_id", referencedColumnName="id")
     **/
    private $animal;

    /**
     * @Orm\ManyToOne(targetEntity="Jalis\Bundle\UserBundle\Entity\User", inversedBy="tenants")
     */
    private $user;

    /**
     * @Orm\ManyToOne(targetEntity="Crija\Bundle\AquariumBundle\Entity\Aquarium", inversedBy="tenants")
     */
    private $aquarium;

    /**
     * @Orm\ManyToOne(targetEntity="Shop", inversedBy="tenants")
     * @Orm\JoinColumn(name="shop_id", referencedColumnName="id")
     **/
    private $shop;




    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set colors
     *
     * @param string $colors
     * @return Tenant
     */
    public function setColors($colors)
    {
        $this->colors = $colors;

        return $this;
    }

    /**
     * Get colors
     *
     * @return string 
     */
    public function getColors()
    {
        return $this->colors;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Tenant
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set inReefAt
     *
     * @param \DateTime $inReefAt
     * @return Tenant
     */
    public function setInReefAt($inReefAt)
    {
        $this->inReefAt = $inReefAt;

        return $this;
    }

    /**
     * Get inReefAt
     *
     * @return \DateTime 
     */
    public function getInReefAt()
    {
        return $this->inReefAt;
    }

    /**
     * Set diedAt
     *
     * @param \DateTime $diedAt
     * @return Tenant
     */
    public function setDiedAt($diedAt)
    {
        $this->diedAt = $diedAt;

        return $this;
    }

    /**
     * Get diedAt
     *
     * @return \DateTime 
     */
    public function getDiedAt()
    {
        return $this->diedAt;
    }

    /**
     * Set size
     *
     * @param string $size
     * @return Tenant
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return string 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return Tenant
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }


    /**
     * @return int
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * @param int $units
     */
    public function setUnits($units)
    {
        $this->units = $units;
    }


    /**
     * Set animal
     *
     * @param \Crija\Bundle\AnimalBundle\Entity\Animal $animal
     * @return Tenant
     */
    public function setAnimal(\Crija\Bundle\AnimalBundle\Entity\Animal $animal = null)
    {
        $this->animal = $animal;

        return $this;
    }

    /**
     * Get animal
     *
     * @return \Crija\Bundle\AnimalBundle\Entity\Animal 
     */
    public function getAnimal()
    {
        return $this->animal;
    }

    /**
     * Set user
     *
     * @param \Jalis\Bundle\UserBundle\Entity\User $user
     * @return Tenant
     */
    public function setUser(\Jalis\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Jalis\Bundle\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set aquarium
     *
     * @param \Crija\Bundle\AquariumBundle\Entity\Aquarium $aquarium
     * @return Tenant
     */
    public function setAquarium(\Crija\Bundle\AquariumBundle\Entity\Aquarium $aquarium = null)
    {
        $this->aquarium = $aquarium;

        return $this;
    }

    /**
     * Get aquarium
     *
     * @return \Crija\Bundle\AquariumBundle\Entity\Aquarium 
     */
    public function getAquarium()
    {
        return $this->aquarium;
    }

    /**
     * Set shop
     *
     * @param \Crija\Bundle\AnimalBundle\Entity\Shop $shop
     * @return Tenant
     */
    public function setShop(\Crija\Bundle\AnimalBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \Crija\Bundle\AnimalBundle\Entity\Shop 
     */
    public function getShop()
    {
        return $this->shop;
    }
}
