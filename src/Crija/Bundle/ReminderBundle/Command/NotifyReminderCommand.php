<?php

namespace Crija\Bundle\ReminderBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
// logger
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class NotifyReminderCommand extends ContainerAwareCommand
{
    

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        ini_set("memory_limit", "-1");
        // 0-A. Initialize objects for command.
        parent::initialize($input, $output);
        $this->em     = $this->getContainer()->get('doctrine')->getManager();
    }

    protected function configure()
    {
        $this
                ->setName('crija:notify-reminder')
                ->setDescription('Daily process for notify reminder events.') 
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
       $notificaciones = array();
       $count = 0;
  
       $entities = $this->em->getRepository('CrijaReminderBundle:Event')->findAll();
       $today    = new \DateTime('now');
       
       foreach ($entities as $event) {

         if($event->getCountDown() <= 0) {

             if($event->getType() == 1 ) {

                 $event->setCompletedAt($today);
             } else {

                 $event->setCompletedAt( new \DateTime($event->getEndDate()));

             }
           $this->em->persist($event);

            $emailfrom = 'hola@mycoralreef.es';
            $emailto   = $event->getUser()->getEmail();
            $subject   = 'Notificacion de MyCoralReef.es';
            $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($emailfrom)
            ->setTo($emailto)
            ->setBody($event->getMessage())
            ;
             $this->getApplication()->getKernel()->getContainer()->get('mailer')->send($message);
           $count++;
           $notificaciones[] = "Notificado evento-".$event->getId()." | ".$event->getMessage(). " al usuario ".$event->getUser()->getUsername()." (".$event->getUser()->getEmail().")";
           
         }

           $this->em->flush();

           if($event->getType() == 1) {

               $this->em->remove($event);
               $this->em->flush();

           }


       }

       $output->writeln("Se han notificado estos eventos:");

       foreach($notificaciones as $notificacion) {
		$output->writeln($notificacion);	
	}
 $output->writeln("Total: ".$count);
    }
    

}

?>
