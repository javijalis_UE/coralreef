<?php

namespace Crija\Bundle\ReminderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EventType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type','choice', array(
                      'choices'   => array('0' => 'Periodica', '1' => 'Puntual'),
                      'required'  => true,
                      'label'=> 'Tipo'
                  ))
            ->add('startedAt','date',array('label' => 'iniciar alerta','data'  => date_create(),'format' => 'dd MM yyyy' ))
            ->add('message')
            ->add('periodicity','choice', array(
                      'choices'   => array('+1 days' => 'diario','+3 days' => '3 dias', '+7 days' => 'Semanal', '+30 days' => 'Mensual', '+60 days' => 'Bimensual', '+180 days' => 'Semestral', '+365 days' => 'Anual'),
                       'required'  => false,
                       'empty_value' => 'Elige periodicidad',
                       'empty_data'  => '+1 days',
                       'label'  => 'Periodicidad'
                  ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Crija\Bundle\ReminderBundle\Entity\Event'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'crija_bundle_reminderbundle_event';
    }
}
