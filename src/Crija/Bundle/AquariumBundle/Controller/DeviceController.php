<?php

namespace Crija\Bundle\AquariumBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Crija\Bundle\AquariumBundle\Entity\Device;
use Crija\Bundle\AquariumBundle\Form\DeviceType;
use Jalis\Bundle\GalleryBundle\Entity\Photo;
use Jalis\Bundle\GalleryBundle\Form\PhotoType;

/**
 * Device controller.
 *
 * @Route("/device")
 */
class DeviceController extends Controller
{

    /**
     * Lists all Device entities.
     *
     * @Route("/", name="device")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        if(!$user) { die("error no user"); }

        $aquariums = $em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($user);

        $entities = $em->getRepository('CrijaAquariumBundle:Device')->findByAquarium($aquariums[0]);


        $dato['total_on']       = 0;
        $dato['total_off']      = 0;
        $dato['total_watios']   = 0;
        $dato['total_euros']    = 0;

        foreach($entities as $entity) {

            if($entity->getEstado() == 1) {

                $dato['total_on']++;
                $consumo_aparato = $entity->getWatios();

                /* hya mas de una unidad*/
                if($entity->getUnidades() > 1) {
                    $consumo_aparato = $consumo_aparato*$entity->getUnidades();
                }

                /* contamos las horas encendido */
                $consumo_aparato = (($consumo_aparato*0.12*$entity->getHoras()*30)/1000);


            } else {
                $consumo_aparato = 0;
                $dato['total_off']++;

            }

            $dato['total_euros']  = $dato['total_euros']+$entity->getPrecio();
            $dato['total_watios'] = $dato['total_watios']+$consumo_aparato;


        }



        $gallery= array();
        foreach($entities as $entity)
        {
            $data = $em->getRepository('JalisGalleryBundle:Photo')->findOneBy(array('obj_id' => $entity->getId(),'obj_type' =>'Device'),array('id' =>'DESC'));
            if($data != null) {
                $gallery[$entity->getId()]= $data->getWebpath();
            } else {
                $gallery[$entity->getId()]= "/frontendbundle/images/pic0".rand(1,4).".jpg";
            }
        }

        return array(
            'entities' => $entities,
            'gallery' => $gallery,
            'data' => $dato,
            'aquariums' => $aquariums
        );
    }
    /**
     * Creates a new Device entity.
     *
     * @Route("/", name="device_create")
     * @Method("POST")
     * @Template("CrijaAquariumBundle:Device:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Device();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('device'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Device entity.
     *
     * @param Device $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Device $entity)
    {
        $securityContext = $this->container->get('security.context');

        $form = $this->createForm(new DeviceType($securityContext), $entity, array(
            'action' => $this->generateUrl('device_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Device entity.
     *
     * @Route("/new", name="device_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Device();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Device entity.
     *
     * @Route("/{id}", name="device_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaAquariumBundle:Device')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Device entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Device entity.
     *
     * @Route("/{id}/edit", name="device_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaAquariumBundle:Device')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Device entity.');
        }

        $photo = new Photo();
        $photo->setObjId($entity->getId());
        $photo->setObjType('Device');
        $form = $this->createForm(new PhotoType(), $photo);


        $gallery = $em->getRepository('JalisGalleryBundle:Photo')->findBy(array('obj_id' => $entity->getId(),'obj_type' =>'Device'));






        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'gallery' => $gallery,
            'form' => $form->createView()
        );
    }

    /**
    * Creates a form to edit a Device entity.
    *
    * @param Device $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Device $entity)
    {
        $securityContext = $this->container->get('security.context');

        $form = $this->createForm(new DeviceType($securityContext), $entity, array(
            'action' => $this->generateUrl('device_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Device entity.
     *
     * @Route("/{id}", name="device_update")
     * @Method("PUT")
     * @Template("CrijaAquariumBundle:Device:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaAquariumBundle:Device')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Device entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('device_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Device entity.
     *
     * @Route("/{id}", name="device_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CrijaAquariumBundle:Device')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Device entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('device'));
    }

    /**
     * Creates a form to delete a Device entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('device_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Creates a new Device entity.
     *
     * @Route("/device_status_update", name="device_status_update")
     * @Method("POST")
     * @Template()
     */
    public function deviceStatusUpdateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $id = $request->request->get('device_id');

        $entity = $em->getRepository('CrijaAquariumBundle:Device')->find($id);

        if($entity->getEstado() == 1) { $status = 0;} else { $status = 1;}

            $entity->setEstado($status);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('device'));

    }
}
