<?php

namespace Crija\Bundle\AquariumBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Crija\Bundle\AquariumBundle\Entity\Aquarium;
use Crija\Bundle\AquariumBundle\Form\AquariumType;
use Jalis\Bundle\GalleryBundle\Entity\Photo;
use Jalis\Bundle\GalleryBundle\Form\PhotoType;
/**
 * Aquarium controller.
 *
 * @Route("/aquarium")
 */
class AquariumController extends Controller
{

    /**
     * Lists all Aquarium entities.
     *
     * @Route("/", name="aquarium")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $entities = $em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($user);

        $gallery= array();
        foreach($entities as $entity)
        {
           $data = $em->getRepository('JalisGalleryBundle:Photo')->findOneBy(array('obj_id' => $entity->getId(),'obj_type' =>'Aquarium'),array('id' =>'DESC'));
            if($data != null) {
                $gallery[$entity->getId()]= $data->getWebpath();
            } else {
                $gallery[$entity->getId()]= "/frontendbundle/images/pic0".rand(1,4).".jpg";
            }
        }


        return array(
            'entities' => $entities,
            'gallery' => $gallery
	
        );
    }
    /**
     * Creates a new Aquarium entity.
     *
     * @Route("/", name="aquarium_create")
     * @Method("POST")
     * @Template("CrijaAquariumBundle:Aquarium:new.html.twig")
     */
    public function createAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        
         if (!$user) {
            throw $this->createNotFoundException('Imposible localizar al usuario');
        }
        
        $entity = new Aquarium();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setUser($user);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('aquarium'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Aquarium entity.
     *
     * @param Aquarium $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Aquarium $entity)
    {
        $form = $this->createForm(new AquariumType(), $entity, array(
            'action' => $this->generateUrl('aquarium_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Aquarium entity.
     *
     * @Route("/new", name="aquarium_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {

        $entity = new Aquarium();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Aquarium entity.
     *
     * @Route("/{id}", name="aquarium_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaAquariumBundle:Aquarium')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Aquarium entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Aquarium entity.
     *
     * @Route("/{id}/edit", name="aquarium_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaAquariumBundle:Aquarium')->find($id);

        $photo = new Photo();
        $photo->setObjId($entity->getId());
        $photo->setObjType('Aquarium');
        $form = $this->createForm(new PhotoType(), $photo);


        $gallery = $em->getRepository('JalisGalleryBundle:Photo')->findBy(array('obj_id' => $entity->getId(),'obj_type' =>'Aquarium'));



        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Aquarium entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'form'        => $form->createView(),
            'gallery'     => $gallery,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Aquarium entity.
    *
    * @param Aquarium $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Aquarium $entity)
    {
        $form = $this->createForm(new AquariumType(), $entity, array(
            'action' => $this->generateUrl('aquarium_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Aquarium entity.
     *
     * @Route("/{id}", name="aquarium_update")
     * @Method("PUT")
     * @Template("CrijaAquariumBundle:Aquarium:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaAquariumBundle:Aquarium')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Aquarium entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('aquarium_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Aquarium entity.
     *
     * @Route("delete/{id}", name="aquarium_delete")
     * @Method("get")
     */
    public function deleteAction(Request $request, $id)
    {

        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CrijaAquariumBundle:Aquarium')->find($id);


        if($user != $entity->getUser()){
            throw $this->createNotFoundException('No puedes realizar esta accion.');
        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Aquarium entity.');
        }
    
        $em->remove($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('aquarium'));
    }

    /**
     * Creates a form to delete a Aquarium entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('aquarium_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Widget
     *
     * @Route("/acuarium_photos/{id}", name="acuarium_photos")
     * @Method("GET")
     * @Template()
     */
    public function widgetAction($id)
    {
        $em = $this->getDoctrine()->getManager();


        $photos = $em->getRepository('JalisGalleryBundle:Photo')->findBy(array('obj_id' => $id,'obj_type' =>'Aquarium'));

        return array(
            'photos' => $photos

        );
    }
}
