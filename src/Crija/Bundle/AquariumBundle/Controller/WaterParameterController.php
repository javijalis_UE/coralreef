<?php

namespace Crija\Bundle\AquariumBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Crija\Bundle\AquariumBundle\Entity\WaterParameter;
use Crija\Bundle\AquariumBundle\Form\WaterParameterType;

/**
 * WaterParameter controller.
 *
 * @Route("/waterparameter")
 */
class WaterParameterController extends Controller
{

    /**
     * Lists all Parameter entities.
     *
     * @Route("/graficas/{id}", name="parameter", defaults={"id" = 0})
     * @Method("GET")
     * @Template()
     */
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        if (!$user) {
            die("error no user");
        }
        $aquariums = $em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($user);

        if ($id == 0 && count($aquariums) > 0) {
            $aquarium_id = $aquariums[0]->getId();
        } else {
            $aquarium_id = $id;
        }

        $extra_data = $em->getRepository('CrijaAquariumBundle:Aquarium')->find($aquarium_id);
        $acuario['id'] = $aquarium_id;
        if ($extra_data){
            $acuario['name'] = $extra_data->getName();

            $sql2 = "SELECT * FROM aquarium q,water_parameter w where q.id =".$aquarium_id." and q.user_id =".$user->getId()." and w.aquarium_id = ".$aquarium_id." AND (w.description not like 'Medicion automatica' OR w.description is NULL)";
            $stmt = $em->getConnection()->prepare($sql2);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $acuario['waterParameters'] = $data;

            $sql2 = "SELECT * FROM aquarium q,water_parameter w where q.id =".$aquarium_id." and  q.user_id =".$user->getId()." and w.aquarium_id = ".$aquarium_id." AND w.description = 'Medicion automatica'";
            $stmt = $em->getConnection()->prepare($sql2);
            $stmt->execute();
            $acuario['waterParametersAutomatic'] = $stmt->fetchAll();

        }
        else {
            $acuario['name'] = "";
        }
        return array(
            'aquariums' => $aquariums,
            'acuario' => $acuario
        );
    }

    /**
     * Widget
     *
     * @Route("/widget_parameter/{id}", name="widget_parameter")
     * @Method("GET")
     * @Template()
     */
    public function widgetAction($id)
    {
        $em = $this->getDoctrine()->getManager();


       // $aquariums = $em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($id);

        $sql2 = "SELECT * FROM aquarium q,water_parameter w where q.user_id =".$id." and w.aquarium_id = q.id AND (w.description != 'Medicion automatica' OR w.description is NULL)";
        $stmt = $em->getConnection()->prepare($sql2);
        $stmt->execute();
        $data = $stmt->fetchAll();



        $aquariums[0]['waterParameters'] = $data;


        $sql2 = "SELECT * FROM aquarium q,water_parameter w where q.user_id =".$id." and w.aquarium_id = q.id AND w.description = 'Medicion automatica'";
        $stmt = $em->getConnection()->prepare($sql2);
        $stmt->execute();
        $aquariums_automatic = $stmt->fetchAll();



        return array(
            'aquariums' => $aquariums,
            'aquariums_automaticos' =>  $aquariums_automatic

        );
    }

    /**
     * Lists all history Parameter entities.
     *
     * @Route("/history/{id}", name="history", defaults={"id" = 0})
     * @Method("GET")
     * @Template()
     */
    public function historyAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $user = $this->get('security.context')->getToken()->getUser();
        if(!$user) { die("error no user"); }

        $aquariums = $em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($user);


        if($id == 0 && count($aquariums) > 0)
        {
            $aquarium_id = $aquariums[0]->getId();
        } else {
            $aquarium_id = $id;
        }

        $acuario = $em->getRepository('CrijaAquariumBundle:Aquarium')->findOneBy(array('id' => $aquarium_id));



        return array(
            'aquariums' => $aquariums,
            'entities' => $acuario
           
        );
    }
     /**
     * widget last parameters.
     *
     * @Route("/last", name="parameter_last")
     * @Template()
     */
    public function lastAction()
    {
     
     $aquariums_array = $this->getLastParameters();

        return array(
            'aquariums'      => $aquariums_array,   
        );
    }

    /**
     * get last parameters.
     */
    private function getLastParameters()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        
        
        $keys = array('salinity','calcium','ph','temperature','magnesium','phosphate','alkalinity','ammonia','silica','iodine','nitrate','nitrite','boron','iron','strontium','potassium');

	    if(!$user) { die("error no user"); }
	    
        $aquariums = $em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($user);
       
        $ultima_medicion = array();

        foreach($aquariums as $aquarium) {
            
                $has_parameters = $em->getRepository('CrijaAquariumBundle:WaterParameter')->findBy(array("aquarium" => $aquarium));
                if(count($has_parameters) > 0)  
                { 
		            foreach($keys as $key) {
		            	$parameters = $em->getRepository('CrijaAquariumBundle:WaterParameter')->getLastValuesByKey($aquarium,$key,2);
		            
		               	$parameter_last[$key] = $parameters[0];
		               	if($parameters[1] != null ) {
		                	$parameter_before[$key] = $parameters[1];
		                } 
		                else  
		                {
			            	$parameter_before[$key] = $parameters[0];    
		                }
			        }
			        $aquariums_array[] = array("last" => $parameter_last,"before" => $parameter_before,"name" => $aquarium->getName());

			    }
		        else {
			         $aquariums_array[] = array("last" => null,"before" => null,"name" => $aquarium->getName());
		        }
       }
 
        return  $aquariums_array;
    }
      /**
     * widget gage.
     *
     * @Route("/gage", name="interval_gage")
     * @Template()
     */
    public function gageAction() {
        $em = $this->getDoctrine()->getManager();

        $interval_value = 0;
        $user = $this->get('security.context')->getToken()->getUser();
        if (!$user) {
          die("error no user");
        }

        $aquariums = $em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($user);

        foreach ($aquariums as $aquarium) {

          $parameters_count = $em->getRepository('CrijaAquariumBundle:WaterParameter')->getIntervalbyMonths($aquarium->getId());
          $interval_value = $interval_value + $parameters_count;
        }
        $interval = round(($interval_value * 100) / 10);

        return array('interval' => $interval);
  }

  /**
     * widget clock
     *
     * @Route("/clock", name="clock")
     * @Template()
     */
    public function clockAction()
    {
        return array( 'ok' => 'ok');
    }
   
    /**
     * widget weather
     *
     * @Route("/weather", name="weather")
     * @Template()
     */
    public function weatherAction()
    {
        return array( 'ok' => 'ok');
    }
    /**
     * Creates a new WaterParameter entity.
     *
     * @Route("/", name="waterparameter_create")
     * @Method("POST")
     * @Template("CrijaAquariumBundle:WaterParameter:new.html.twig")
     */
    public function createAction(Request $request)
    {

        $entity = new WaterParameter();
        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $user = $this->get('security.context')->getToken()->getUser();
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->createImageWidget($user);

            return $this->redirect($this->generateUrl('panel_home'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }
    /**
     * create image
     */
    private function createImageWidget($user)
    {
        $filepath = "widgets/".$user->getId();

        if(!is_dir($filepath)) {
            mkdir($filepath, 0777, true);
        }

        $output = shell_exec('wkhtmltoimage http://mycoralreef.es/my_widget/'.$user->getId().' '.$filepath.'/last.png');
    }
    /**
     * Creates a form to create a WaterParameter entity.
     *
     * @param WaterParameter $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(WaterParameter $entity)
    {
       $securityContext = $this->container->get('security.context');

        $form = $this->createForm(new WaterParameterType($securityContext), $entity, array(
            'action' => $this->generateUrl('waterparameter_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new WaterParameter entity.
     *
     * @Route("/new", name="waterparameter_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        if(!$user) { die("error no user"); }

        $aquariums = $em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($user);

        if(!$aquariums) {
            return $this->redirect($this->generateUrl('aquarium_new'));
        }
        $entity = new WaterParameter();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a WaterParameter entity.
     *
     * @Route("/show/{id}", name="waterparameter_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
ldd("SHOW");
        $entity = $em->getRepository('CrijaAquariumBundle:WaterParameter')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find WaterParameter entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing WaterParameter entity.
     *
     * @Route("/{id}/edit", name="waterparameter_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaAquariumBundle:WaterParameter')->find($id);

        $user_logged = $this->get('security.context')->getToken()->getUser();


        $user_entity = $entity->getAquarium()->getUser();


        if ($user_logged->getId() != $user_entity->getId() ) {
            die("error no user");
        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find WaterParameter entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a WaterParameter entity.
    *
    * @param WaterParameter $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(WaterParameter $entity)
    {

   
      $securityContext = $this->container->get('security.context');
        $form = $this->createForm(new WaterParameterType($securityContext), $entity, array(
            'action' => $this->generateUrl('waterparameter_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing WaterParameter entity.
     *
     * @Route("/{id}/update", name="waterparameter_update")
     * @Method("POST")
     * @Template("CrijaAquariumBundle:WaterParameter:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');
        $entity = $em->getRepository('CrijaAquariumBundle:WaterParameter')->find($id);
        $user = $this->get('security.context')->getToken()->getUser();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find WaterParameter entity.');
        }


        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new WaterParameterType($securityContext), $entity);
        $editForm->bind($request);

       // if ($editForm->isValid()) {

            $em->persist($entity);
            $em->flush();
           $this->createImageWidget($user);
            return $this->redirect($this->generateUrl('history'));
        //}


        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a WaterParameter entity.
     *
     * @Route("/delete/{id}", name="waterparameter_delete")
     */
    public function deleteAction(Request $request, $id)
    {


        $user_logged = $this->get('security.context')->getToken()->getUser();



            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CrijaAquariumBundle:WaterParameter')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find WaterParameter entity.');
            }

            $user_entity = $entity->getAquarium()->getUser();
            if ($user_logged->getId() != $user_entity->getId() ) {
                die("error no user");
            }

            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('history'));
    }

    /**
     * Creates a form to delete a WaterParameter entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('waterparameter_delete', array('id' => $id)))
            //->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Parameters alerts.
     *
     * @Route("/my/alerts", name="parameter_alerts")
     * @Template()
     */
    public function alertsAction()
    {
	    $alerts = array();
      
        $aquariums = $this->getLastParameters();

        if($aquariums) {
            foreach ($aquariums as $aquarium) {
                if ($aquarium['last']['temperature']['temperature'] > 27.5) {
                    $alerts[] = array('icon' => "wi wi-thermometer", 'text' => "Temperatura alta en el acuario ".$aquarium['name'], 'class' => "font-size:70px;");
                }
                if ($aquarium['last']['temperature']['temperature'] < 24.5) {
                    $alerts[] = array('icon' => "wi wi-thermometer-exterior", 'text' => "Temperatura baja en el acuario ".$aquarium['name'], 'class' => "font-size:70px;");
                }
                if ($aquarium['last']['calcium']['calcium'] < 400) {
                    $alerts[] = array('icon' => "fa fa-eyedropper", 'text' => "Falta calcio en el acuario ".$aquarium['name'], 'class' => "font-size:48px;padding:12px;");
                }
                if ($aquarium['last']['magnesium']['magnesium'] < 1200) {
                    $alerts[] = array('icon' => "fa fa-eyedropper", 'text' => "Falta magnesio en el acuario ".$aquarium['name'], 'class' => "font-size:48px;padding:12px;");
                }
                if ($aquarium['last']['salinity']['salinity'] < 1020) {
                    $alerts[] = array('icon' => "wi wi-sprinkles", 'text' => "Salinidad baja en el acuario ".$aquarium['name'], 'class' => "font-size:70px;");
                }
                if ($aquarium['last']['alkalinity']['alkalinity'] < 5) {
                    $alerts[] = array('icon' => "wi wi-sprinkles", 'text' => "Carbonatos bajos en el acuario ".$aquarium['name'], 'class' => "font-size:70px;");
                }
            }
        }
        return array('alerts' =>$alerts);
    }




}
