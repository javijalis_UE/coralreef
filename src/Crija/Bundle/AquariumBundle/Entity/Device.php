<?php

namespace Crija\Bundle\AquariumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Device
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Crija\Bundle\AquariumBundle\Entity\DeviceRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Device
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="marca", type="string", length=255)
     */
    private $marca;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo", type="string", length=255)
     */
    private $modelo;

    /**
     * @var integer
     *
     * @ORM\Column(name="watios", type="integer", nullable=true)
     */
    private $watios;

    /**
     * @var integer
     *
     * @ORM\Column(name="precio", type="integer", nullable=true)
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="tienda", type="string", length=255, nullable=true)
     */
    private $tienda;

    /**
     * @var integer
     *
     * @ORM\Column(name="unidades", type="integer", nullable=true)
     */
    private $unidades;

    /**
     * @var integer
     *
     * @ORM\Column(name="HORAS", type="integer", nullable=true)
     */
    private $horas;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean", nullable=true)
     */
    private $estado;



    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string")
     */
    private $tipo;

    /**
     * created Time/Date
     *
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * updated Time/Date
     *
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;


    /**
     * @Orm\ManyToOne(targetEntity="Aquarium", inversedBy="water_parameters")
     */
    private $aquarium;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marca
     *
     * @param string $marca
     * @return Device
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return string 
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set modelo
     *
     * @param string $modelo
     * @return Device
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return string 
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set watios
     *
     * @param integer $watios
     * @return Device
     */
    public function setWatios($watios)
    {
        $this->watios = $watios;

        return $this;
    }

    /**
     * Get watios
     *
     * @return integer 
     */
    public function getWatios()
    {
        return $this->watios;
    }

    /**
     * Set precio
     *
     * @param integer $precio
     * @return Device
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return integer 
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Device
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tienda
     *
     * @param string $tienda
     * @return Device
     */
    public function setTienda($tienda)
    {
        $this->tienda = $tienda;

        return $this;
    }

    /**
     * Get tienda
     *
     * @return string 
     */
    public function getTienda()
    {
        return $this->tienda;
    }

    /**
     * @return int
     */
    public function getHoras()
    {
        return $this->horas;
    }

    /**
     * @param int $horas
     */
    public function setHoras($horas)
    {
        $this->horas = $horas;
    }

    /**
     * Set unidades
     *
     * @param integer $unidades
     * @return Device
     */
    public function setUnidades($unidades)
    {
        $this->unidades = $unidades;

        return $this;
    }

    /**
     * Get unidades
     *
     * @return integer 
     */
    public function getUnidades()
    {
        return $this->unidades;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Device
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }



    /**
     * Set aquarium
     *
     * @param \Crija\Bundle\AquariumBundle\Entity\Aquarium $aquarium
     * @return WaterParameter
     */
    public function setAquarium(\Crija\Bundle\AquariumBundle\Entity\Aquarium $aquarium = null)
    {
        $this->aquarium = $aquarium;

        return $this;
    }

    /**
     * Get aquarium
     *
     * @return \Crija\Bundle\AquariumBundle\Entity\Aquarium
     */
    public function getAquarium()
    {
        return $this->aquarium;
    }
    /**
     * @return boolean
     */
    public function isTipo()
    {
        return $this->tipo;
    }

    /**
     * @param boolean $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }
}
