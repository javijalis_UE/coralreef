<?php

namespace Jalis\Bundle\UserBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        
        $builder->add('firstName', 'text', array('required' => false ,'label' => 'Nombre'));
        $builder->add('lastName', 'text', array('required' => false ,'label' => 'Apellidos'));

        $builder->add('address', 'text', array('required' => false ,'label' => 'Direccion'));
        $builder->add('cp', 'text', array('required' => false ,'label' => 'Codigo Postal'));
  
    }

    public function getName()
    {
        return 'jalis_user_registration';
    }
  
}


