<?php
// src/Jalis/Bundle/UserBundle/Entity/User.php

namespace Jalis\Bundle\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Orm\Column(type="string", name="first_name", nullable=true)
     */
    private $firstName;

    /**
     * @Orm\Column(type="string", name="last_name", nullable=true)
     */
    private $lastName;
   
     /**
     * @Orm\Column(type="integer", name="cp", nullable=true)
     */
    private $cp;

    /**
     * @Orm\Column(type="string", name="address", nullable=true)
     */
    private $address;

    /**
     * @Orm\Column(type="string", name="ws_token", nullable=true)
     */
    private $wsToken;


    /**
     * @Orm\OneToMany(targetEntity="\Crija\Bundle\AquariumBundle\Entity\Aquarium", mappedBy="User", cascade={"persist"})
     */
    protected $aquarium;
    
    /**
     * @Orm\OneToMany(targetEntity="\Crija\Bundle\ReminderBundle\Entity\Event", mappedBy="User", cascade={"persist"})
     */
    protected $event;

    /**
     * @Orm\OneToMany(targetEntity="\Jalis\Bundle\CommentBundle\Entity\Comment", mappedBy="User", cascade={"persist"})
     */
    protected $comment;


    

     /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->aquariums = new \Doctrine\Common\Collections\ArrayCollection();
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set cp
     *
     * @param integer $cp
     * @return User
     */
    public function setCp($cp)
    {
        $this->cp = $cp;
    
        return $this;
    }

    /**
     * Get cp
     *
     * @return integer 
     */
    public function getCp()
    {
        return $this->cp;
    }
    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        $this->address = $address;
    }
    
    /**
     * Add aquarium
     *
     * @param \Crija\Bundle\AquariumBundle\Entity\Aquarium $aquarium
     * @return User
     */
    public function addAquarium(\Crija\Bundle\AquariumBundle\Entity\Aquarium $aquarium)
    {
        $this->aquiarium[] = $aquarium;
    
        return $this;
    }

    /**
     * Remove aquarium
     *
     * @param \Crija\Bundle\AquariumBundle\Entity\Aquarium $aquarium
     */
    public function removeAquarium(\Crija\Bundle\AquariumBundle\Entity\Aquarium $aquarium)
    {
        $this->aquarium->removeElement($aquarium);
    }

    /**
     * Get aquarium
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getAquarium()
    {
        return $this->aquarium;
    }
    
    
    
    /**
     * Add event
     *
     * @param \Crija\Bundle\ReminderBundle\Entity\Event $event
     * @return User
     */
    public function addEvent(\Crija\Bundle\ReminderBundle\Entity\Event $event)
    {
        $this->event[] = $event;
    
        return $this;
    }

    /**
     * Remove event
     *
     * @param \Crija\Bundle\ReminderBundle\Entity\Event $event
     */
    public function removeEvent(\Crija\Bundle\ReminderBundle\Entity\Event $event)
    {
        $this->event->removeElement($event);
    }

    /**
     * Get event
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Add comment
     *
     * @param \Jalis\Bundle\CommentBundle\Entity\Commnet $comments
     * @return User
     */
    public function addComment(\Jalis\Bundle\CommentBundle\Entity\Comment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \Jalis\Bundle\CommentBundle\Entity\Commnet $comments
     */
    public function removeComment(\Jalis\Bundle\CommentBundle\Entity\Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get event
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getComment()
    {
        return $this->comments;
    }

    /**
     * @return mixed
     */
    public function getWsToken()
    {
        return $this->wsToken;
    }

    /**
     * @param mixed $wsToken
     */
    public function setWsToken($wsToken)
    {
        $this->wsToken = $wsToken;
    }


}
