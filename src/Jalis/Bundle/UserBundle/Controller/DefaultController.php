<?php

namespace Jalis\Bundle\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Process\Process;
use Symfony\Component\Filesystem\Filesystem;
use Jalis\Bundle\CommentBundle\Entity\Comment;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Route("/usuario/{nick}",name="public_user_show")
     * @Template()
     */
    public function publicUserShowAction($nick)
    {
        $em   = $this->getDoctrine()->getManager();
        $user = $em->getRepository('JalisUserBundle:User')->findOneBy(array('username' => $nick));

        $this->generateQr($user);

        $aquarium = $em->getRepository('CrijaAquariumBundle:Aquarium')->findOneByUser($user);



        $sql2 = "SELECT * FROM aquarium q,water_parameter w where q.user_id =".$user->getId()." and w.aquarium_id = q.id AND w.description = 'Medicion automatica'";
        $stmt = $em->getConnection()->prepare($sql2);
        $stmt->execute();
        $aquariums_automatic = $stmt->fetchAll();


        return array(
            'user' => $user,
            'aquarium' => $aquarium,
            'aquariums_automaticos' =>  $aquariums_automatic
        );
    }


    /**
     * generate qr
     */
    public function generateQr($user)
    {
        $fs = new Filesystem();

        $dataToQr = "http://www.mycoralreef.es/usuario/".$user->getUsername();


        $qr_path = "/var/www/vhosts/mycoralreef.es/httpdocs/web/qr/";

        try {
            $fs->mkdir($qr_path);
        } catch (IOExceptionInterface $e) {
            echo "An error occurred while creating your directory at ".$e->getPath();
        }
        if (!file_exists($qr_path.$user->getId().".png")) {

            $generated = null;

            if($dataToQr != null) {

                $process = new Process("qrencode -o ".$qr_path.$user->getId().".png '".$dataToQr."'");
                $process->run();

                // executes after the command finishes
                if (!$process->isSuccessful()) {
                    throw new \RuntimeException($process->getErrorOutput());
                }


            }

        }

    }
    /**
     * @Route("/timeline/new_comment",name="new_timeline_comment")
     * @Template()
     */
    public function TimelineCommentAction(Request $request)
    {
      if($user = $this->get('security.context')->getToken()->getUser())
      {
            $em   = $this->getDoctrine()->getManager();

            $texto  =$request->request->get('comment');

            $comment = new Comment();

            $comment->setText($texto);
            $comment->setObjId(null);
            $comment->setObjType('timeline');
            $comment->setUser($user);

            $em->persist($comment);
            $em->flush();

            return $this->redirect($this->generateUrl('timeline',array('nick' => $user->getUsername())));

        } else {

            throw $this->createAccessDeniedException();
        }
    }

    /**
     * @Route("/timeline",name="timeline")
     * @Template()
     */
    public function timelineAction()
    {
        $this->em = $this->get('doctrine')->getManager();

        $user =  $this->get('security.context')->getToken()->getUser();


        $sql2 = "(SELECT 'parameter' as type,w.id,w.created_at as created,aq.user_id as user,w.description as description
        from water_parameter w, aquarium aq where w.aquarium_id  = aq.id and w.description != 'Medicion automatica' and aq.user_id = ".$user->getId().")
        union
        (SELECT 'tenant' as type,id,createdAt as created,user_id as user,null as description from Tenant where user_id = ".$user->getId().")
        union
        (SELECT 'died' as type,id,died_at as created,user_id as user,null as description from Tenant where user_id = ".$user->getId()." AND died_at is not null)
         union
        (SELECT 'device' as type,d.id,d.created_at as created,aq.user_id as user,CONCAT(marca,' ',modelo) as description from Device d,aquarium aq where d.aquarium_id  = aq.id and aq.user_id = ".$user->getId().")
         union
        (SELECT 'timeline' as type,id,created_at as created,user_id as user,text as description from Comment where user_id = ".$user->getId()." AND obj_type = 'timeline')
        order by created DESC";
        $stmt = $this->em->getConnection()->prepare($sql2);
        $stmt->execute();
        $timeline_data = $stmt->fetchAll();

        foreach($timeline_data as $data) {

            if($data['type'] == 'tenant') {

                $data['obj'] =  $this->em->getRepository('CrijaAnimalBundle:Tenant')->findOneBy(array('id' => $data['id']));

            }
            if($data['type'] == 'died') {

                $data['obj'] =  $this->em->getRepository('CrijaAnimalBundle:Tenant')->findOneBy(array('id' => $data['id']));

            }
            if($data['type'] == 'device') {

                $data['obj'] =  $this->em->getRepository('CrijaAquariumBundle:Device')->findOneBy(array('id' => $data['id']));
                $photo = $this->em->getRepository('JalisGalleryBundle:Photo')->findOneBy(array('obj_id' => $data['id'],'obj_type' => 'device'));

                if($photo != null) {
                    $data['img']= $photo->getWebpath();
                } else {
                    $data['img'] = "/frontendbundle/images/pic0".rand(1,4).".jpg";
                }


            }
            if($data['type'] == 'parameter') {
                $data['obj'] =  $this->getDoctrine()
                    ->getRepository('CrijaAquariumBundle:WaterParameter')
                    ->createQueryBuilder('e')
                    ->select('e')
                    ->where('e.id =:userPosting')->setParameter('userPosting', $data['id'])
                    ->getQuery()
                    ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);


                //  $data['obj'] =  $this->em->getRepository('CrijaAquariumBundle:WaterParameter')->findOneBy(array('id' => $data['id']))->toArray();
            }
            $timeline[] = $data;

        }

        $aquariums =  $this->em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($user);

        return array(
            'timeline' => $timeline,
            'user' => $user,
            'user_loged' => $user_loged,
            'aquarium' => $aquariums[0]
        );
    }


    /**
     * @Route("/timelinePublic/{nick}",name="timeline_public")
     * @Template()
     */
    public function timelinePublicAction($nick)
    {
        $this->em = $this->get('doctrine')->getManager();
        $user = $this->em->getRepository('JalisUserBundle:User')->findOneBy(array('username' => $nick));



        $sql2 = "(SELECT 'parameter' as type,w.id,w.created_at as created,aq.user_id as user,w.description as description
        from water_parameter w, aquarium aq where w.aquarium_id  = aq.id and w.description != 'Medicion automatica' and aq.user_id = ".$user->getId().")
        union
        (SELECT 'tenant' as type,id,createdAt as created,user_id as user,null as description from Tenant where user_id = ".$user->getId().")
        union
        (SELECT 'died' as type,id,died_at as created,user_id as user,null as description from Tenant where user_id = ".$user->getId()." AND died_at is not null)
         union
         (SELECT 'device' as type,d.id,d.created_at as created,aq.user_id as user,CONCAT(marca,' ',modelo) as description from Device d,aquarium aq where d.aquarium_id  = aq.id and aq.user_id = ".$user->getId().")
         union
        (SELECT 'timeline' as type,id,created_at as created,user_id as user,text as description from Comment where user_id = ".$user->getId()." AND obj_type = 'timeline')
        order by created DESC";
        $stmt = $this->em->getConnection()->prepare($sql2);
        $stmt->execute();
        $timeline_data = $stmt->fetchAll();

        foreach($timeline_data as $data) {

            if($data['type'] == 'tenant') {

                $data['obj'] =  $this->em->getRepository('CrijaAnimalBundle:Tenant')->findOneBy(array('id' => $data['id']));

            }
            if($data['type'] == 'died') {

                $data['obj'] =  $this->em->getRepository('CrijaAnimalBundle:Tenant')->findOneBy(array('id' => $data['id']));

            }
            if($data['type'] == 'device') {

                $data['obj'] =  $this->em->getRepository('CrijaAquariumBundle:Device')->findOneBy(array('id' => $data['id']));
                $photo = $this->em->getRepository('JalisGalleryBundle:Photo')->findOneBy(array('obj_id' => $data['id'],'obj_type' => 'device'));

                if($photo != null) {
                    $data['img']= $photo->getWebpath();
                } else {
                    $data['img'] = "/frontendbundle/images/pic0".rand(1,4).".jpg";
                }


            }
            if($data['type'] == 'parameter') {
                $data['obj'] =  $this->getDoctrine()
                    ->getRepository('CrijaAquariumBundle:WaterParameter')
                    ->createQueryBuilder('e')
                    ->select('e')
                    ->where('e.id =:userPosting')->setParameter('userPosting', $data['id'])
                    ->getQuery()
                    ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);


                //  $data['obj'] =  $this->em->getRepository('CrijaAquariumBundle:WaterParameter')->findOneBy(array('id' => $data['id']))->toArray();
            }
            $timeline[] = $data;

        }

        $aquariums =  $this->em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($user);

        return array(
            'timeline' => $timeline,
            'user' => $user,
            'user_loged' => $user_loged,
            'aquarium' => $aquariums[0]
        );
    }

    /**
     * @Route("/devicePublic/{nick}",name="device_public")
     * @Template()
     */
    public function devicePublicAction($nick)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('JalisUserBundle:User')->findOneBy(array('username' => $nick));

        if(!$user) { die("error no user"); }

        $aquariums = $em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($user);


        $entities = $em->getRepository('CrijaAquariumBundle:Device')->findByAquarium($aquariums[0]);


        $dato['total_on'] = 0;
        $dato['total_off'] = 0;
        $dato['total_watios'] = 0;
        $dato['total_euros'] = 0;

        foreach($entities as $entity) {
            if($entity->getEstado() == 1) {

                $dato['total_on']++;
                $consumo_aparato = $entity->getWatios();

                /* hya mas de una unidad*/
                if($entity->getUnidades() > 1) {
                    $consumo_aparato = $consumo_aparato*$entity->getUnidades();
                }

                /* contamos las horas encendido */
                $consumo_aparato = (($consumo_aparato*0.12*$entity->getHoras()*30)/1000);


            } else {
                $consumo_aparato = 0;
                $dato['total_off']++;

            }

            $dato['total_euros']  = $dato['total_euros']+$entity->getPrecio();
            $dato['total_watios'] = $dato['total_watios']+$consumo_aparato;
        }

        $dato['total_watios'] = (($dato['total_watios']*0.12*24*30)/1000);

        $gallery= array();
        foreach($entities as $entity)
        {
            $data = $em->getRepository('JalisGalleryBundle:Photo')->findOneBy(array('obj_id' => $entity->getId(),'obj_type' =>'Device'),array('id' =>'DESC'));
            if($data != null) {
                $gallery[$entity->getId()]= $data->getWebpath();
            } else {
                $gallery[$entity->getId()]= "/frontendbundle/images/pic0".rand(1,4).".jpg";
            }
        }

        return array(
            'entities' => $entities,
            'gallery' => $gallery,
            'user' => $user,
            'data' => $dato
        );
    }


}
