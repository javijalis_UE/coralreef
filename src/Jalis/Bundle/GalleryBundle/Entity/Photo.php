<?php

namespace Jalis\Bundle\GalleryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
/**
 * Jalis\Bundle\GalleryBundle\Entity\Photo
 *
 * @ORM\Table(name ="photo")
 * @ORM\Entity(repositoryClass="Jalis\Bundle\GalleryBundle\Entity\PhotoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Photo
{
    // a property used temporarily while deleting
    private $filenameForRemove;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $path
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255,nullable=true)
     */
    private $title;

    /**
     * @var integer $obj_id
     *
     * @ORM\Column(name="obj_id", type="integer")
     */
    private $obj_id;

    /**
     * @var integer $obj_type
     *
     * @ORM\Column(name="obj_type", type="string", length=255)
     */
    private $obj_type;

    /**
     * @Assert\File(maxSize="6000000")
     */
    public $file;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    public function getWebPath()
    {

        return null === $this->path ? null : $this->getUploadDir().'/'.$this->obj_type.'/'.$this->obj_id.'/'.$this->id.'_'.$this->path;
    }

    protected function getUploadRootDir()
    {

        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/galleries';
    }
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->path =  $this->file->getClientOriginalName();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $file_to_upload = $this->id.'_'.$this->file->getClientOriginalName();
        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->file->move($this->getUploadRootDir().'/'.$this->obj_type.'/'.$this->obj_id , $file_to_upload);

        unset($this->file);
    }

    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove()
    {
        $this->filenameForRemove = $this->getAbsolutePath();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($this->filenameForRemove) {
            unlink($this->filenameForRemove);
        }
    }

    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->obj_type.'/'.$this->obj_id.'/'.$this->id.'_'.$this->path;
    }



    /**
     * Set path
     *
     * @param string $path
     * @return Photo
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->id.'_'.$this->path;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Photo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set obj_id
     *
     * @param integer $objId
     * @return Photo
     */
    public function setObjId($objId)
    {
        $this->obj_id = $objId;

        return $this;
    }

    /**
     * Get obj_id
     *
     * @return integer
     */
    public function getObjId()
    {
        return $this->obj_id;
    }

    /**
     * Set obj_type
     *
     * @param integer $objType
     * @return Photo
     */
    public function setObjType($objType)
    {
        $this->obj_type = $objType;

        return $this;
    }

    /**
     * Get obj_type
     *
     * @return integer
     */
    public function getObjType()
    {
        return $this->obj_type;
    }
}
