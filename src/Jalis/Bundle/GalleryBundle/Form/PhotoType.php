<?php

namespace Jalis\Bundle\GalleryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PhotoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('file')
            ->add('obj_type', 'hidden')
            ->add('obj_id', 'hidden')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Jalis\Bundle\GalleryBundle\Entity\Photo'
        ));
    }

    public function getName()
    {
        return 'jalis_gallerybundle_phototype';
    }
}
