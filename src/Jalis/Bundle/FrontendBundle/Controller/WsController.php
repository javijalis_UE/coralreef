<?php

namespace Jalis\Bundle\FrontendBundle\Controller;

use Crija\Bundle\AquariumBundle\Entity\WaterParameter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class WsController extends Controller
{
    /**
     * @Route("/ws/new_parameter",name="new_parameter")
     * @Template()
     */
    public function generateTokenAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $token = $request->query->get('token');
        $ph = $request->query->get('ph');
        $temperature = $request->query->get('temperature');

        $user = $em->getRepository('JalisUserBundle:User')->findOneBy(array('wsToken' => $token));
        $aquarium = $em->getRepository('CrijaAquariumBundle:Aquarium')->findOneBy(array('user' => $user));
        $last_parameter = $em->getRepository('CrijaAquariumBundle:WaterParameter')->findOneBy(array('aquarium' => $aquarium, 'description' => 'Medicion automatica'), array('id' => 'DESC'));

        $error = null;

        $date1 = new \DateTime();
        if ($last_parameter) {
            $date2 = $last_parameter->getCreatedAt();
        } else {
            $date2 = new \DateTime('2013-01-29');


        }

        $diff = $date2->diff($date1);



        $horas = $diff->format('%h');
        $dia =  $diff->format('%a');

        if($horas < 1 && $dia == 0) {
            $error = array('cod' => 1 , 'msg' => 'Solo se permite una medicion automatica cada hora, ultima medicion hace '.$horas.' horas y '.$dia.' dias');
        }



        if($error == null) {

            $newParameter = new WaterParameter();

            $newParameter->setPh($ph);
            $newParameter->setTemperature($temperature);
            $newParameter->setDescription('Medicion automatica');
            $newParameter->setAquarium($aquarium);
            $newParameter->setCreatedAt($date1);
            $newParameter->setUpdatedAt($date1);


            $em->persist($newParameter);
            $em->flush();


            $data = array('cod' => 0 , 'msg' => 'Sin errores, medicion automatica anterior hace '.$horas.' horas y '.$dia.' dias');
            return new JsonResponse($data);
        } else {
            return new JsonResponse($error);
        }


    }
}
