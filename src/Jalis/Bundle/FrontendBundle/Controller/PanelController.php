<?php

namespace Jalis\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Process\Process;
use Symfony\Component\Filesystem\Filesystem;

class PanelController extends Controller
{
    /**
     * @Route("/panel",name="panel_home")
     * @Template()
     */
    public function indexAction()
    {
        $this->em = $this->get('doctrine')->getManager();
        $user = $this->get('security.context')->getToken()->getUser();

        $aquariums = $this->em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($user);

        $sql = "SELECT count(id) AS numero
                        FROM Tenant
                        WHERE user_id = " . $user->getId();

        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        $tenants = $stmt->fetchColumn();


        if ($aquariums) {

        $sql2 = "SELECT count(id) AS numero
                        FROM water_parameter
                        WHERE aquarium_id = " . $aquariums[0]->getId();

        $stmt = $this->em->getConnection()->prepare($sql2);
        $stmt->execute();
        $parameters = $stmt->fetchColumn();

            $sql2 =  "SELECT count(id) AS numero
                        FROM Device
                        WHERE aquarium_id = " . $aquariums[0]->getId();

            $stmt = $this->em->getConnection()->prepare($sql2);
            $stmt->execute();
            $devices = $stmt->fetchColumn();

         }

        $sql2 =  "SELECT count(id) AS numero
                        FROM event
                        WHERE user_id = ".$user->getId();

        $stmt = $this->em->getConnection()->prepare($sql2);
        $stmt->execute();
        $alerts = $stmt->fetchColumn();




        $this->generateQr($user);

        return array(
            'user' => $user,
            'name' => 'portada',
            'parameters' => $parameters,
            'alerts' => $alerts,
            'devices' => $devices,
            'tenants' => $tenants

        );

    }
    /**
     * @Route("/test",name="panel_test")
     * @Template()
     */
    public function testAction()
    {

        $user = $this->get('security.context')->getToken()->getUser();



        return array(
            'user' => $user,
            'name' => 'portada'
        );

    }
    /**
     * generate qr
     */
    public function generateQr($user)
    {
        $fs = new Filesystem();

        $dataToQr = "http://www.mycoralreef.es/usuario/".$user->getUsername();


        $qr_path = "/var/www/vhosts/mycoralreef.es/httpdocs/web/qr/";

        try {
            $fs->mkdir($qr_path);
        } catch (IOExceptionInterface $e) {
            echo "An error occurred while creating your directory at ".$e->getPath();
        }
        if (!file_exists($qr_path.$user->getId().".png")) {

         $generated = null;

            if($dataToQr != null) {

                $process = new Process("qrencode -o ".$qr_path.$user->getId().".png '".$dataToQr."'");
                $process->run();

                // executes after the command finishes
                if (!$process->isSuccessful()) {
                    throw new \RuntimeException($process->getErrorOutput());
                }


            }

        }

    }
    /**
     * @Route("/tools",name="panel_tools")
     * @Template()
     */
    public function toolsAction()
    {
        $this->em = $this->get('doctrine')->getManager();
        $user = $this->get('security.context')->getToken()->getUser();



        return array(
            'user' => $user

        );

    }
    /**
     * @Route("/generate_ws_token",name="generate_ws_token")
     * @Template()
     */
    public function generateTokenAction()
    {
        $em = $this->get('doctrine')->getManager();
        $user = $this->get('security.context')->getToken()->getUser();


        $token= md5(uniqid(rand(), true));
        
        $user->setWsToken($token);
        $em->persist($user);
        $em->flush();


        return $this->redirect($this->generateUrl('panel_tools'));

    }


}
