My Coral Reef
========================

Symfony 2.14

* Fase 1: core,acuarios, parametros y graficas
* Fase2: eventos e inquilinos,expertos y cometarios
* Fase3: ws moviles y monetizables + paypal y microtrasacciones
## Normas de convivencia: ##

- Cada uno que coga la issue que mas le guste.
- Si se te ocurre algo que no esta en las issues lo metes en issues y te la asignas o no te la asignas y la dejas por si alguien quiere hacerla
- Cuando se coga una issue hay que asignarsela a uno mismo para que dos personas no esten con la misme issue
- No se peude coger una issue que ya este asiganda 

## admin user ##

* usuario: admin
* password: password

## entities ##

* aquiarium: user_id, name , litres, aquarium_type,descripction
* parameters: aquarium_id, calcium, Alkalinity,Salinity, temperature, ph, Magnesium,Phosphate,Ammonia,Silica,Iodine,nitrate,nitrite,Strontium,Boron,Iron created_at, updated_at,description

@todo

* Log parametros acuario
* Galerias de fotos? acurio nm fotos, pez nm fotos
* 1 usuario puede tener varias peceras
* graficas del log de aprametros
* version movil (phonagap) (monetizable)
* eventos y alertas de usuario
* un acuario puede tener varios inquilinos (plantas, peces, invertebrados, corales...)
* base de datos de peces
* compatibilidad de inquilinos (alertas de incompatibilidad)
* widget para compartir tus parametros en foros y webs
* log de medicamentos (habria que estudiar como van estas cosas)
* calculadoras? si tengo una cuario de 50x50x60 cuanta watios de luz necesito?
* alertas basados en el weather? si hay ola de calor alertas apra tomar medidas en el acuario
* WS server para conexion de Arduinos u otros para el log de parametros
* bundle comentarios para el log y peces
* ranking de los mejores acuarios/usuarios
* rol "experto" para hacer consultas/preguntas (monetizable)